import os
import pickle

def load_dss(BASE_DIR, main_dss):
    main_dss_absolute = os.path.join(BASE_DIR, main_dss)
    if os.path.isfile(main_dss_absolute):
        DS_SETTINGS_FILE = open(main_dss_absolute, 'rb')
        DSS = pickle.load(DS_SETTINGS_FILE)
        DS_SETTINGS_FILE.close()

        for dss_src in DSS.get('load_dss'):
            dss_src_absolute = os.path.join(BASE_DIR, dss_src)
            DSS_LOAD = open(dss_src_absolute, 'rb')
            dss = pickle.load(DSS_LOAD)
            if DSS['settings'].get('INSTALLED_APPS'):
                DSS['settings']['INSTALLED_APPS'] += dss['settings'].get('INSTALLED_APPS', [])
            else:
                DSS['settings']['INSTALLED_APPS'] = dss['settings'].get('INSTALLED_APPS', [])
            if DSS['settings'].get('LOCALE_PATHS'):
                DSS['settings']['LOCALE_PATHS'] += [local_path for local_path in dss['settings'].get('LOCALE_PATHS', [])]
            else:
                DSS['settings']['LOCALE_PATHS'] = [local_path for local_path in dss['settings'].get('LOCALE_PATHS', [])]
            DSS['settings']['DJANGOCMS_FORMS_TEMPLATES'] = dss['settings'].get('DJANGOCMS_FORMS_TEMPLATES')
            DSS['settings']['NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS'] = dss['settings'].get('NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS')
            DSS['settings']['TEMPLATES'] = dss['settings'].get('TEMPLATES')
            DSS['settings']['BLOG_IMAGE_FULL_SIZE'] = dss['settings'].get('BLOG_IMAGE_FULL_SIZE')
            DSS['settings']['BLOG_IMAGE_THUMBNAIL_SIZE'] = dss['settings'].get('BLOG_IMAGE_THUMBNAIL_SIZE')
            DSS['settings']['BLOG_LATEST_POSTS'] = dss['settings'].get('BLOG_LATEST_POSTS')
            DSS['settings']['BLOG_PLUGIN_TEMPLATE_FOLDERS'] = dss['settings'].get('BLOG_PLUGIN_TEMPLATE_FOLDERS')
            DSS['settings']['CMS_TEMPLATES'] = dss['settings'].get('CMS_TEMPLATES')
            DSS['settings']['CKEDITOR_SETTINGS'] = dss['settings'].get('CKEDITOR_SETTINGS')
            DSS['settings']['DJANGOCMS_ICON_SETS'] = dss['settings'].get('DJANGOCMS_ICON_SETS')
            DSS['settings']['HAS_PURCHASED_THEME_LICENCE'] = dss['settings'].get('HAS_PURCHASED_THEME_LICENCE')
            DSS_LOAD.close()
    else:
        DSS = None
    return DSS