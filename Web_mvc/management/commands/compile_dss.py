from django.core.management.base import BaseCommand, CommandError
import os
import pickle
from django.conf import settings
from django.conf.urls import include, url
from theme.settings import THEME_DSS

class Command(BaseCommand):
    help = 'Compile dynamicly stored settings (DSS)'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        if settings.DS_SETTINGS == None:
            DS_SETTINGS = {'settings':{'INSTALLED_APPS':["theme"]}, 'load_dss':["theme/settings.dss"], 'urlpatterns':[]}
            main_file = open(os.path.join(settings.BASE_DIR, settings.DS_SETTINGS_FILENAME), 'wb')
            pickle.dump(DS_SETTINGS, main_file)
            main_file.close()

        theme_dss_file = open(os.path.join(settings.BASE_DIR, "theme/settings.dss"), 'wb')
        pickle.dump(THEME_DSS, theme_dss_file)
        theme_dss_file.close()
        self.stdout.write(self.style.SUCCESS('Successfully compiled DSS'))
