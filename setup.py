import warnings

from setuptools import setup, find_packages

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None


setup(
    name='web-mvc-pro-base-shipping-modifier',
    version="1.0",
    description=(
        'Django-SHOP base shipping modifier.'
    ),
    long_description=README,
    install_requires=REQUIREMENTS,
    author='Danny Waser',
    author_email='danny@waser.tech',
    url='https://gitlab.com/waser-technologies/web.mvc-pro-modifiers/shipping/web.mvc-pro-base-shipping-modifier',
    packages=find_packages(exclude=("tests", "test_project", "Web_mvc")),
    include_package_data=True,
    classifiers=[
        'Development Status :: 6 - Mature',
        'Environment :: Web Environment',
        'Framework :: Django, Django-SHOP',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Utilities'
    ],
)
